package events.controllers.events;


import events.services.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class CreateEventController {
    private final EventService service;

    public CreateEventController(EventService service) {
        this.service = service;
    }

    @PostMapping("/events")
    public String create(@RequestParam Map<String, String> params) {
        service.save(params);

        return "redirect:/events";
    }
}
