package events.controllers.events;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class GetEventTemplateController {

    @GetMapping("/add/event")
    public String get() {

        return "add-event";
    }
}
