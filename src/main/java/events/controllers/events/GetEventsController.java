package events.controllers.events;

import events.models.Event;
import events.services.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class GetEventsController {
    private final EventService service;

    public GetEventsController(EventService service) {
        this.service = service;
    }

    @GetMapping("/events")
    public String get(Model model) {
        List<Event> events = service.findAll();
        model.addAttribute("events", events);
        return "events";
    }
}
