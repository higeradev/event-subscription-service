package events.controllers.subscriptions;

import events.services.SubscriptionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CreateSubscriptionController {
    private final SubscriptionService service;

    public CreateSubscriptionController(SubscriptionService service) {
        this.service = service;
    }

    @PostMapping("/subscriptions/{eventId}")
    public String create(@PathVariable String eventId, @RequestParam String email) {

        if (service.isExist(email)) {
            if (service.isSubscribed(email, eventId)) {
                return "existed-subscription";
            }
            else {
                service.addEvent(email, eventId);
            }
        }
        else {
            service.save(email, eventId);
        }

        return "redirect:/subscriptions/email";
    }
}
