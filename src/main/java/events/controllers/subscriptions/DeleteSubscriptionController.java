package events.controllers.subscriptions;

import events.services.SubscriptionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DeleteSubscriptionController {
    private final SubscriptionService service;

    public DeleteSubscriptionController(SubscriptionService service) {
        this.service = service;
    }

    @PostMapping("/subscriptions/delete/{eventId}")
    public String create(@PathVariable String eventId, @RequestParam String email) {

        service.deleteEvent(email, eventId);

        return "redirect:/subscriptions/email";
    }
}
