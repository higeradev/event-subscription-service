package events.controllers.subscriptions;

import events.models.Subscription;
import events.services.SubscriptionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class GetSubscriptionController {
    private final SubscriptionService service;

    public GetSubscriptionController(SubscriptionService service) {
        this.service = service;
    }

    @GetMapping("/subscriptions")
    public String get(@RequestParam String email, Model model) {
        Subscription subscription = service.findByEmail(email);
        model.addAttribute("subscription", subscription);
        return "subscription";
    }
}
