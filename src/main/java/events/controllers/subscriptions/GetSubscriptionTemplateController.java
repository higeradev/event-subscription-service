package events.controllers.subscriptions;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class GetSubscriptionTemplateController {

    @GetMapping("/subscriptions/email")
    public String get() {

        return "email-subscription";
    }
}
