package events.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "events")
public class Event {
    @Id
    private String id;

    private String title;
    private String date;
    private String time;
    private String description;

    public void setId() {
        this.id = UUID.randomUUID().toString();
    }

}
