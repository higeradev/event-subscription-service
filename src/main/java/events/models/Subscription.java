package events.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "subscriptions")
public class Subscription {
    @Id
    private String id;

    @DBRef
    private List<Event> events;

    private String email;
    private String registrationTime;

    public void setId() {
        this.id = UUID.randomUUID().toString();
    }

    public void setRegistrationTime() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm");
        this.registrationTime = now.format(formatter);
    }

    public void addEvent(Event event) {
        this.events.add(event);
    }
}
