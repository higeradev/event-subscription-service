package events.repositories;

import events.models.Subscription;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionRepository extends CrudRepository<Subscription, String> {
    @Query("{}")
    List<Subscription> getAll();
    Subscription findByEmail(String email);
}
