package events.services;

import events.models.Event;

import java.util.List;
import java.util.Map;

public interface EventService {
    void save(Map<String, String> eventParams);
    List<Event> findAll();
    Event findById(String eventId);

}
