package events.services;

import events.models.Subscription;

import java.util.List;

public interface SubscriptionService {
    void save(String email, String eventId);
    List<Subscription> findAll();
    Subscription findById(String subscriptionId);
    void delete(String subscriptionId);
    boolean isSubscribed (String email, String eventId);
    boolean isExist(String email);
    Subscription findByEmail(String email);
    void addEvent(String email, String eventId);
    void deleteEvent(String email, String eventId);
}
