package events.services.impl;

import events.models.Event;
import events.repositories.EventRepository;
import events.services.EventService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {
    private final EventRepository repository;

    public EventServiceImpl(EventRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(Map<String, String> eventParams) {
        System.out.println("date " + eventParams.get("date"));
        Event event = Event
                        .builder()
                        .title(eventParams.get("title"))
                        .date(eventParams.get("date"))
                        .time(eventParams.get("time"))
                        .description(eventParams.get("description"))
                        .build();
        event.setId();

        repository.save(event);
    }

    @Override
    public List<Event> findAll() {
        return repository.getAll();
    }

    @Override
    public Event findById(String eventId) {
        Optional<Event> event = repository.findById(eventId);

        if (event.isPresent()) {
            return event.get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event with such ID doesn't exist!");
    }
}
