package events.services.impl;

import events.models.Event;
import events.models.Subscription;
import events.repositories.SubscriptionRepository;
import events.services.EventService;
import events.services.SubscriptionService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
    private final SubscriptionRepository subscriptionRepository;
    private final EventService eventService;

    public SubscriptionServiceImpl(SubscriptionRepository subscriptionRepository, EventService eventService) {
        this.subscriptionRepository = subscriptionRepository;
        this.eventService = eventService;
    }

    @Override
    public void save(String email, String eventId) {

        Event event = eventService.findById(eventId);

            Subscription subscription = Subscription
                    .builder()
                    .events(new ArrayList<>())
                    .email(email)
                    .build();

            subscription.setId();
            subscription.setRegistrationTime();
            subscription.addEvent(event);

            subscriptionRepository.save(subscription);

    }

    @Override
    public List<Subscription> findAll() {
        return subscriptionRepository.getAll();
    }

    @Override
    public Subscription findById(String subscriptionId) {
        Optional<Subscription> subscription = subscriptionRepository.findById(subscriptionId);

        if (subscription.isPresent()) {
            return subscription.get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Subscription with such ID doesn't exist!");
    }

    @Override
    public void delete(String subscriptionId) {
        subscriptionRepository.deleteById(subscriptionId);
    }

    @Override
    public boolean isSubscribed(String email, String eventId) {
        Subscription existedAccount = subscriptionRepository.findByEmail(email);

        List<Event> events = existedAccount.getEvents();
        Optional<Event> subscribedEvent = events.stream().filter(it -> it.getId().equals(eventId)).findAny();

        return subscribedEvent.isPresent();
    }

    @Override
    public boolean isExist(String email) {
        Subscription existedAccount = subscriptionRepository.findByEmail(email);

        return existedAccount != null;
    }

    @Override
    public Subscription findByEmail(String email) {
        return subscriptionRepository.findByEmail(email);
    }

    @Override
    public void addEvent(String email, String eventId) {
        Subscription existedAccount = subscriptionRepository.findByEmail(email);
        Event event = eventService.findById(eventId);

        existedAccount.addEvent(event);

        subscriptionRepository.save(existedAccount);
    }

    @Override
    public void deleteEvent(String email, String eventId) {
        Subscription subscription = subscriptionRepository.findByEmail(email);
        List<Event> events = subscription.getEvents();

        events.removeIf(it -> it.getId().equals(eventId));
        subscription.setEvents(events);

        subscriptionRepository.save(subscription);
    }
}
