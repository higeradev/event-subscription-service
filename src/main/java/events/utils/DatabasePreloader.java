package events.utils;

import events.models.Event;
import events.repositories.EventRepository;
import events.repositories.SubscriptionRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;
import java.util.stream.Stream;

@Configuration
public class DatabasePreloader {

    @Bean
    CommandLineRunner initDatabase(EventRepository repository, SubscriptionRepository subscriptionRepository) {
        repository.deleteAll();
        subscriptionRepository.deleteAll();

        return (args) -> Stream.of(events())
                .peek(System.out::println)
                .forEach(repository::save);
    }

    private Event[] events() {
        return new Event[] {
                new Event(UUID.randomUUID().toString(), "Json Derulo concert", "2023-11-04", "20:00", "Место проведения: г. Алматы, Алматы Арена"),
                new Event(UUID.randomUUID().toString(),"«Best of season» Ne Prosto Orchestra", "2023-12-02", "19:00", "Место проведения: г. Алматы, Алматы Арена"),
                new Event(UUID.randomUUID().toString(),"Вилли Вонка (2023)", "2023-12-13", "18:00", "Фильм «Вилли Вонка» - захватывающее приключение и фантазия, рассказывающая историю эксцентричного кондитера Вилли Вонки и его загадочной шоколадной фабрики."),

        };
    }
}
